$(document).ready(function () {
  var searchPhrases = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: 'https://dev-ofre.lxa.fr/api/lgr/suggest?q=__QUERY__',
      wildcard: '__QUERY__'
    }
  });
  $('#search .typeahead').typeahead({
    hint: true,
    highlight: false,
    minLength: 1,
    classNames: {
      dataset: 'list-group',
      selectable: 'list-group-item'
    }
  }, {
    limit: 20,
    name: 'search-phrases',
    display: 'uri',
    source: searchPhrases,
    templates: {
      suggestion: Handlebars.compile('<div><span class="glyphicon glyphicon-{{icon}}" aria-hidden="true" /> {{title}} - <code>{{uri}}</code></div>')
    }
  });
  $('#search .typeahead').bind('typeahead:select', function (ev, suggestion) {
    url = '/' + suggestion.uri;
    window.location.replace(url);
  });
  $('.twitter-typeahead').removeAttr('style');
});